module Main where

import GlossNetwork
import GlossInput
import GlossGraphics

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game (Key(..))
import Reactive.Banana

type Score = (Integer, Integer)

-- Some rendering functions

paddlePicture :: Point -> Picture
paddlePicture (x,y) = translate x y (color red rect)
  where rect = rectangleSolid 10 150

ballPicture :: Point -> Picture
ballPicture (x, y) = translate x y (color white rect)
  where rect = rectangleSolid 10 10

scorePicture :: Score -> Picture
scorePicture (oneScore, twoScore) = translate (-100) (-300) $ scale 0.3 0.3 $ color white scoreText
  where scoreText = text $ (show oneScore) ++ " vs " ++ (show twoScore)

-- utility functions to increase readability

add :: Point -> Point -> Point
add (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

pickB :: Behavior t Bool -> a -> a -> Behavior t a
pickB p c1 c2 = fmap pick p
  where pick p' = if p' then c1 else c2

-- Physics

-- Forces point to lie between two values
clamp :: Ord a => a -> a -> a -> a
clamp lower upper x = min upper (max lower x)

clamp2D :: (Float, Float) -> -- Horizontal boundary
           (Float, Float) -> -- Vertical boundary
           Point -> Point
clamp2D (hlower, hupper) (vlower, vupper) (x, y) =
  (clamp hlower hupper x, clamp vlower vupper y)

data Exit = E | NE | N | NW | W | SW | S | SE
          deriving (Eq, Ord, Enum)

fromChecks :: Bool -> Bool -> Bool -> Bool -> Maybe Exit
fromChecks e n w s
  | e &&     n = Just NE
  | e &&     s = Just S
  | e && not w = Just E
  | w &&     n = Just NW
  | w &&     s = Just SW
  | w && not e = Just W
  | n && not s = Just N
  | s && not n = Just S
  | otherwise = Nothing

normal :: Exit -> Float
normal e = case e of
            E  -> pi
            NE -> 5*pi/4
            N  -> 3*pi/2
            NW -> 6*pi/4
            W  -> 0
            SW -> pi/4
            S  -> pi/2
            SE -> 3*pi/4

-- This the "physics" of the game, given an initial position and a velocity it
-- returns the stream of (integrated) positions and a events for when something
-- hits the border of the screen (this is all the collision detection I can be
-- bothered with implementing right now).
physics :: GlossInput t ->                  -- Input streams
           Point ->                         -- Starting value
           Behavior t Point ->              -- Velocity
           (Behavior t Point, Event t Exit) -- Positions and sequence of exits
physics input init velocity = (position, filterJust exits)
    where update (vx, vy) d (cx, cy) = checkExit (cx + vx * d, cy + vy * d)
          checkExit (x, y) =
            -- First component checks each boundary
            ( fromChecks (x > 512) (y > 384) (x < -512) (y < -384)
            -- Force the new value to be inside the screen
            , clamp2D (-511, 511) (-383, 383) (x, y) )
          -- Accumulate the changes
          (exits, position) = mapAccum init (update <$> velocity <@> ticks input)

-- Game Logic

baseVelocity = 400

-- Creates a stream of positions controlled by `up` and `down` (i.e. a stream of
-- Floats that is increasing while `up` is held down, decreasing while `down` is
-- held down).
playerPaddle :: GlossInput t -> -- Input streams
                Float -> -- Initial vertical position
                Key -> Key -> -- Up/Down keys
                Behavior t Point -- Resulting positions
playerPaddle input horizonal up down = position
  where upPressed   = keyPressed input up
        downPressed = keyPressed input down
        -- Paddle velocity is increased by one while up is pressed and decreased
        -- by one while down is pressed (so 0 if both are pressed)
        velocity = add <$>
                   pickB upPressed   (0,  baseVelocity) (0, 0)  <*>
                   pickB downPressed (0, -baseVelocity) (0, 0)
        (position, _) = physics input (horizonal, 0) velocity

-- Reflect an angle around a normal
reflect :: Float -> Float -> Float
reflect about angle = fmod (2*about - pi - angle)
  where fmod a | 0 <= a && a < 2*pi = a
               | a < 0 = fmod (a + 2*pi)
               | 2*pi <= a = fmod (a - 2*pi)

-- Angular coordinates in R^2
angular :: Float -> Float -> Point
angular mag arg = (mag * cos arg, mag * sin arg)

-- A position that changes with a fixed velocity along an angle, with the angle
-- bouncing when it hits a corner
ball :: GlossInput t ->                  -- Input streams
        Float ->                         -- Initial angle
        (Behavior t Point, Event t Exit) -- Position and exit events
ball input initialAngle = (position, exits)
  where angle = accumB initialAngle reflects
        velocity = fmap (angular baseVelocity) angle
        (position, exits) = physics input (0, 0) velocity
        reflects = fmap (\e -> reflect (normal e)) exits

-- This is the "actual" game

bumpLeft, bumpRight :: (Integer, Integer) -> (Integer, Integer)
bumpLeft  (a, b) = (a + 1, b)
bumpRight (a, b) = (a, b + 1)

pong :: GlossNetwork
pong input = defaultOutputs { pics = pics }
  where p1Position = playerPaddle input (-505) (Char 'w') (Char 's')
        p2Position = playerPaddle input   505  (Char 'o') (Char 'l')
        (bPosition, ballExit) = ball input 0.3
        -- Compute scoring
        playerScore = accumB (0, 0) $ union
                      (pure bumpLeft  <@ p1Goal)
                      (pure bumpRight <@ p2Goal)
        -- Each time the ball exits check whether the exit is a scoring one
        p1Goal = whenE (missed <$> p2Position <*> bPosition) (filterE (==E) ballExit)
        p2Goal = whenE (missed <$> p1Position <*> bPosition) (filterE (==W) ballExit)
        missed (_, py) (_, by) = abs (py - by) > 75
        -- Combine things into a picture
        pics = let p1 = paddlePicture <$> p1Position
                   p2 = paddlePicture <$> p2Position
                   b  = ballPicture   <$> bPosition
                   s  = scorePicture  <$> playerScore
               in overlayB [p1, p2, b, s]

-- Setup and plumbing

main = let displayMode = InWindow "Stupid Pong" (1024, 768) (0, 1)
       in playNetwork displayMode black 30 noHooks pong
