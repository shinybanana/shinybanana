module GlossGraphics (overlayB) where

import Data.Traversable (sequenceA)

import Graphics.Gloss
import Reactive.Banana

overlayB :: [Behavior t Picture] -> Behavior t Picture
overlayB pics = pictures <$> sequenceA pics
