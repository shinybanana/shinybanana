module GlossInput (GlossInput, glossInput, events, ticks, keyPressed, keypress) where

import Reactive.Banana

import Graphics.Gloss.Interface.IO.Game hiding (Event)
import qualified Graphics.Gloss.Interface.IO.Game as Gloss (Event)

data GlossInput t = GlossInput { events :: Event t Gloss.Event
                               , ticks  :: Event t Float }

glossInput :: Event t Gloss.Event -> Event t Float -> GlossInput t
glossInput = GlossInput

keyPressed :: GlossInput t -> Key -> Behavior t Bool
keyPressed input key = stepper False changes
  where relevant (EventKey key' _ _ _) = key == key'
        relevant _ = False
        interpret (EventKey _ Up _ _)   = False
        interpret (EventKey _ Down _ _) = True
        changes = fmap interpret $ filterE relevant (events input)

keypress :: GlossInput t -> Key -> Event t ()
keypress input key = fmap (const ()) $ filterE relevant (events input)
  where relevant (EventKey key' _ _ _) = key == key'
        relevant _ = False
