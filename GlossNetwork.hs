{-# LANGUAGE RankNTypes #-}
module GlossNetwork (GlossNetwork, playNetwork, noHooks, GlossOutputs(..), defaultOutputs) where

import Control.Applicative ((<$>), (<*>))

import GlossInput (GlossInput, glossInput)

import Graphics.Gloss.Interface.IO.Game hiding (Event)
import qualified Graphics.Gloss.Interface.IO.Game as Gloss (Event)

import Reactive.Banana

import Reactive.Banana.Frameworks
import Control.Event.Handler

import Control.Concurrent (yield, forkIO)
import Control.Concurrent.MVar (MVar, newMVar, readMVar, swapMVar)

type GlossNetwork = forall t. Frameworks t =>
                    GlossInput t ->
                    GlossOutputs t

data GlossOutputs t = GlossOutputs { pics  :: Behavior t Picture
                                   , done  :: Behavior t Bool
                                   , debug :: Event t String }

defaultOutputs :: GlossOutputs t
defaultOutputs = GlossOutputs (stepper blank never) (stepper False never) never

type EventHook  = Gloss.Event -> IO (Maybe Gloss.Event)
type TickHook   = Float -> IO (Maybe Float)
type RenderHook = IO (Maybe Picture)

data Hooks = Hooks { eventHook  :: Maybe EventHook
                   , tickHook   :: Maybe TickHook
                   , renderHook :: Maybe RenderHook }

noHooks :: Hooks
noHooks = Hooks Nothing Nothing Nothing

exit :: IO ()
exit = undefined

playNetwork :: Display -> Color -> Int -> Hooks -> GlossNetwork -> IO ()
playNetwork disp col hz hooks game = do
  (handleEvent, fireEvent) <- newAddHandler
  (handleTick, fireTick) <- newAddHandler
  picCache <- newMVar blank
  let network' :: forall t. Frameworks t => Moment t ()
      network' = do
        eEvent <- fromAddHandler handleEvent
        eTick <- fromAddHandler handleTick
        let g = game (glossInput eEvent eTick)
        reactimate $ fmap (\w -> swapMVar picCache w >> yield) ((pics g) <@ eTick)
        reactimate $ fmap (const exit) $ filterE id ((done g) <@ eTick)
        reactimate $ fmap (putStrLn) $ debug g
  network <- compile network'
  let onEvent = case eventHook hooks of
                 Nothing -> \event _ ->
                   fireEvent event
                 Just hook -> \event _ ->
                   maybe (return ()) fireEvent =<< hook event
      onTick = case tickHook hooks of
                 Nothing -> \tick _ ->
                   fireTick tick
                 Just hook -> \tick _ ->
                   maybe (return ()) fireTick =<< hook tick
      onRender = case renderHook hooks of
                  Nothing -> const $ readMVar picCache
                  Just hook -> const $ maybe <$> readMVar picCache <*> pure id <*> hook
  _ <- forkIO $ actuate network
  playIO disp col hz () onRender onEvent onTick
